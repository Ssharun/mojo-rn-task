import { StatusBar } from "expo-status-bar";
import { SafeAreaView, StyleSheet, Text } from "react-native";
import Stack from "./components/Stack";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <Stack direction="row" style={styles.blockSpacingSmall}>
        <Text>1</Text>
        <Text>2</Text>
        <Text>3</Text>
      </Stack>
      <Stack direction="row" style={styles.block}>
        <Text>1</Text>
        <Text>2</Text>
        <Text>3</Text>
      </Stack>
      <Stack direction="row" style={styles.blockSpacingLarge}>
        <Text>1</Text>
        <Text>2</Text>
        <Text>3</Text>
      </Stack>
      <Stack direction="row" style={styles.blockLastItem}>
        <Text>1</Text>
        <Text>2</Text>
        <Text>3</Text>
      </Stack>
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const SPACING = {
  small: 10,
  regular: 20,
  large: 30,
};

const LINE_HEIGHT = 1.4;

const FONT_SIZES = {
  small: 12,
  regular: 16,
  large: 18,
  heading: 24,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  content: {
    width: "100%",
    maxWidth: 800,
    padding: SPACING.large,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  logo: {
    width: 131,
    height: 44,
  },
  main: {
    backgroundColor: "#F6F6F6",
    borderRadius: 12,
    padding: SPACING.regular,
  },
  text: {
    fontSize: FONT_SIZES.regular,
    lineHeight: FONT_SIZES.regular * LINE_HEIGHT,
    textAlign: "left",
  },
  textSmall: {
    fontSize: FONT_SIZES.small,
    lineHeight: FONT_SIZES.small * LINE_HEIGHT,
  },
  textLarge: {
    fontSize: FONT_SIZES.large,
    lineHeight: FONT_SIZES.large * LINE_HEIGHT,
  },
  textHeading: {
    fontSize: FONT_SIZES.heading,
    lineHeight: FONT_SIZES.heading * LINE_HEIGHT,
    fontWeight: "bold",
  },
  textBold: {
    fontWeight: "bold",
  },
  textItalic: {
    fontStyle: "italic",
  },
  link: {
    color: "blue",
    textDecorationLine: "underline",
  },
  // Replace with <Stack /> component?
  block: {
    marginBottom: SPACING.regular,
    alignSelf: "stretch",
    justifyContent: "center",
  },
  blockSpacingSmall: {
    marginBottom: SPACING.small,
  },
  blockSpacingLarge: {
    marginBottom: SPACING.large,
  },
  blockLastItem: {
    marginBottom: 0,
  },
});