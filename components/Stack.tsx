import React, { FC } from 'react';
import { View, ViewStyle } from 'react-native';
type Direction = 'row' | 'column'
type Align = 'center' | 'flex-start' | 'flex-end' | 'baseline' | 'stretch'
type Justify = 'center' | 'flex-start' | 'flex-end' | 'space-around' | 'space-between' | 'space-evenly';
type Props = {
  children: React.ReactNode;
  direction?: Direction;
  justify?: Justify;
  align?: Align;
  style?: ViewStyle;
}
const Stack: FC<Props> = ({ children, direction = 'column', justify = "flex-start", align = 'flex-start', style, ...props }) => {
  const items = React.Children.map(children, (child, index) => {
    if (!React.isValidElement(child)) {
      return child;
    }
    const mb = style?.marginBottom || 0;
    const childStyle = direction === 'column' ? { marginBottom: mb } : { marginRight: mb };
    const isLastChild = index === React.Children.count(children) - 1;
    return (
      <View style={!isLastChild ? childStyle : null}>
        {child}
      </View>
    );
  });
  return (
    <View style={[{ flexDirection: direction, justifyContent: justify, alignItems: align }, style]} {...props}>
      {items}
    </View>
  )
}
export default Stack;







